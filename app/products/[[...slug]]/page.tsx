import React from 'react';

interface Props {
    params: {
        slug: string[]; // dinamyc segment (we dont know the segname name ahead of time)
    };
    //query string eg : '/products/groceries?sortOder=name'
    searchParams: {
        sortOrder: string
    }
}

function ProductPage({ params, searchParams }: Props) {
    // 
    return (
        <>
            <div>ProductPage</div>
            <div>{params.slug}</div>
            <div>{searchParams.sortOrder}</div>
        </>
    );
}

export default ProductPage;
