'use client'
import React from 'react'

interface Props {
    error: Error,
    reset: ()=>void
}

function ErrorPage({error, reset}: Props) { // next js automatically passes these props
    console.log("Error:", error);
    
  return (
    <>
        <div>An unexpected error has ocurred</div>
        <button className='btn' onClick={reset}>Retry</button>
    </>
  )
}

export default ErrorPage