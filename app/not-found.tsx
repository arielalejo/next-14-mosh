import React from 'react'

function NotFound() {
  return (
    <div>Page cannot be found</div>
  )
}

export default NotFound