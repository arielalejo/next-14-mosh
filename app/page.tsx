import Image from 'next/image'
import Link from 'next/link'
import ProductCart from './components/ProductCart/ProductCart'

export default function Home() {
  console.log("test 1 - ServerSide compont")
  return (
    <main>
      <h1>hello world</h1>
      <Link href="/users">users</Link>
      <ProductCart/>
    </main>
  )
}
