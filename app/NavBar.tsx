import React from 'react'
import Link from 'next/link';

function NavBar() {
  return (
    <nav className='flex bg-green-50 py-5'>
        <Link className='ml-5' href="/">Home</Link>
        <Link className='ml-5' href="/users">Users</Link>
        <Link className='ml-5' href="/admin">Admin</Link>
    </nav>
  )
}

export default NavBar