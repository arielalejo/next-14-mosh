import React from 'react';
import UserTable from './UserTable';
import Link from 'next/link';

interface Props{
    searchParams :{
        sortOrder: string
    }
}

const UsersPage = async ({searchParams}:Props) => {
    console.log(searchParams.sortOrder); //will log in the server console
    
    return (
        <>
            <h1>Users</h1> {/* Doesnt have the native h1 styles, because of tailwind, we should edit globals.css and override the style for h1 manually */}
            <Link href="/users/new" className='btn'>New User</Link>
            <UserTable sortOrder={searchParams.sortOrder}/>
        </>
    )
};

export default UsersPage;
