'use client'

import React from 'react';
import { useRouter } from 'next/navigation';

function NewUserPage() {
    const router = useRouter();

    const handleClick = () => {
        router.push("/")
    }
    return (
        <div>
            <div>NewUser Page</div>
            <button className='btn btn-primary' onClick={handleClick}>Create User</button>
        </div>
    );
}

export default NewUserPage;
