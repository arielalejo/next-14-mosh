import React from 'react'

interface Props {
    params:{
        id:number
    }
}

function UserDetailPage({params}:Props) { // name of prop 'params' is NECESSARY FOR PAGES, and it should contain the dynamic path ( in this case 'id')
 // this approach only works for PAGES
  return (
    <div>UserDetailPage {params.id}</div>
  )
}

export default UserDetailPage