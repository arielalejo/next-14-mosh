import React from 'react'

interface Props {
    // the page can read all the top level dynamic routes
    // in this case [id] [photoId]
    params: {
        id: number;  // for /users/[id]  path
        photoId: number //for /photos/[photoId] path
    }
}

function PhotoDetail({params}:Props) {
  return (
    <div>PhotoDetail {params.id} {params.photoId}</div>
  )
}

export default PhotoDetail